program project1;
const
  MatrixSize = 9;
  MaxMatrixElement = 13;
  MinMatrixElement = -15;

type
  MatrixType = array [1..MatrixSize,1..MatrixSize] of Integer;

procedure CreateMatrix(var matrix: MatrixType);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            matrix[i, j] := Random(MaxMatrixElement - MinMatrixElement + 1) + MinMatrixElement;
        end;
    end;
end;

procedure WriteMatrix(var matrix: MatrixType);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            Write(matrix[i, j]:5);
        end;
        WriteLn;
    end;
    WriteLn;
end;

function GetCountNegeativeElementsOnMainDiagonal(var matrix: MatrixType): Integer;
var
  i, j, count: Integer;
begin
    count := 0;
    for i := 1 to MatrixSize do
    begin
        if matrix[i,i] < 0 then
        begin
                count := count + 1;
        end;
    end;
    GetCountNegeativeElementsOnMainDiagonal := count;
end;

procedure ReplacePositiveElements(var matrix: MatrixType);
var
    i, j: Integer;
begin
   for i := 1 to MatrixSize do
   begin
       for j := 1 to MatrixSize do
       begin
           if (i < j) and (matrix[i, j] > 0) then
           begin
               matrix[i, j] := 0;
           end;
       end;
   end;
end;

var
    matrix: MatrixType;


begin
    Randomize;
    CreateMatrix(matrix);
    WriteLn('Matrix:');
    WriteMatrix(matrix);
    WriteLn(GetCountNegeativeElementsOnMainDiagonal(matrix));
    WriteLn('Changed matrix');
    ReplacePositiveElements(matrix);
    WriteMatrix(matrix);
    ReadLn;

end.
